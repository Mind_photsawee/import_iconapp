//
//  ViewController.swift
//  import_iconApp
//
//  Created by Admin on 13/3/2562 BE.
//  Copyright © 2562 KMUTNB. All rights reserved.
//

import UIKit
import iOSDropDown

class ViewController: UIViewController {

    @IBOutlet weak var dropDown: DropDown!
    @IBOutlet weak var valueLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        dropDown.optionArray = ["Egg", "Chicken", "Fish"]
        //Its Id Values and its optional
        dropDown.optionIds = [1,23,54,22]
        // The the Closure returns Selected Index and String
        dropDown.didSelect{(selectedText , index ,id) in
            self.valueLabel.text = selectedText

        }
    }
}
